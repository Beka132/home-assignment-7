package com.example.davaleba7

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.davaleba7.databinding.ItemLayoutBinding

class RecyclerViewAdapter(private val items:MutableList<ItemModel>):RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.ItemViewHolder {
        val itemView=ItemLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val holder = ItemViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter.ItemViewHolder, position: Int) {
        val model=items[position]
        holder.binding.ivImage.setImageResource(model.image)
        holder.binding.tvTitle.text=model.title
        holder.binding.tvDescription.text=model.description
    }

    override fun getItemCount(): Int=items.size

    inner class ItemViewHolder(val binding: ItemLayoutBinding):RecyclerView.ViewHolder(binding.root){
    }
}