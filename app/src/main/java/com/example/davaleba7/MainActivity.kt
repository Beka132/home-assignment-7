package com.example.davaleba7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.davaleba7.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var adapter:RecyclerViewAdapter
    private val items= mutableListOf<ItemModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        setData()
    }

    private fun init(){
        binding.include.ivExit.setOnClickListener {
            onDestroy()
        }
        adapter=RecyclerViewAdapter(items)
        binding.recyclerView.layoutManager=LinearLayoutManager(this)
        binding.recyclerView.adapter=adapter
    }

    private fun setData(){
        items.add(
            ItemModel(
                R.drawable.car,
                "car",
                "Default Transport"
            )
        )
        items.add(
            ItemModel(
                R.drawable.police_car,
                "Police car",
                "Policemen's Transport"
            )
        )
        items.add(
            ItemModel(
                R.drawable.ambulance,
                "Ambulance",
                "Transport for EMTs or people who require medical assistance"
            )
        )
        items.add(
            ItemModel(
                R.drawable.fire_truck,
                "Fire Truck",
                "Firefighters' Transport"
            )
        )
        items.add(
            ItemModel(
                R.drawable.taxi,
                "Cab",
                "Transport for Taxi Drivers"
            )
        )
        items.add(
            ItemModel(
                R.drawable.bus,
                "Bus",
                "Transport for Bus Drivers and Passengers"
            )
        )
        items.add(
            ItemModel(
                R.drawable.airplane,
                "Airplane",
                "Transport for Pilots and its Travelers"
            )
        )
        items.add(
            ItemModel(
                R.drawable.helicopter,
                "Helicopter",
                "Transport for Pilots "
            )
        )
        items.add(
            ItemModel(
                R.drawable.space_rocket,
                "Space Rocket",
                "Transport for Astronauts"
            )
        )
        adapter.notifyItemInserted(items.size-1)

    }

    override fun onDestroy() {
        super.onDestroy()
    }
}